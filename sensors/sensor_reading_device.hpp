#pragma once
#include <string>
#include <memory>
namespace sensors
{

#pragma db object
class sensor_reading_device {

public:
#pragma db id
	std::string textual_id_;

	template < class Archive >
	void serialize(Archive & ar) {
		ar(textual_id_);
	}

};

using sensor_reading_device_ptr = std::shared_ptr<sensor_reading_device>;

} // ns sensors
