#pragma once
#include "sensor_reading_device.hpp"
//#include <chrono>
#include "boost/date_time/posix_time/posix_time.hpp"

// serialization
#include <sstream>
namespace sensors
{

#pragma db value
	struct temperature_reading_id {
		std::string sensor_reading_device_id_;

#pragma db sqlite:type("TEXT") mysql:type("TIMESTAMP") not_null
		boost::posix_time::ptime timestamp_;


		bool operator<(temperature_reading_id const & other) const {
			if (sensor_reading_device_id_ < other.sensor_reading_device_id_) {
				return true;
			}
			else if (sensor_reading_device_id_ > other.sensor_reading_device_id_) {
				return false;
			}
			else {
				return timestamp_ < other.timestamp_;
			}
		}



	};
#pragma db object
class temperature_reading {

public:
//	std::chono::time_point timestamp_; // no chrono support yet for ODB

#pragma db id
	temperature_reading_id temperature_reading_id_;

	bool sensor_online_;
	float temperature_;

		std::string const &sensor_reading_device_id() const {
			return temperature_reading_id_.sensor_reading_device_id_;
		}

		void sensor_reading_device_id(std::string const &sensor_reading_device_id) {
			temperature_reading_id_.sensor_reading_device_id_ = sensor_reading_device_id;
		}

		boost::posix_time::ptime const &timestamp() const {
			return temperature_reading_id_.timestamp_;
		}

		void timestamp(boost::posix_time::ptime const &timestamp) {
			temperature_reading_id_.timestamp_ = timestamp;
		}
	bool sensor_online() const {
		return sensor_online_;
	}

	void sensor_online(bool sensor_online) {
		sensor_online_ = sensor_online;
	}

	float temperature() const {
		return temperature_;
	}

	void temperature(float temperature) {
		temperature_ = temperature;
	}

	template < class Archive >
	void save(Archive & archive) const {
		archive(sensor_reading_device_id());
		archive(temperature_);
		// timestamp
		std::string timestamp_string = boost::posix_time::to_simple_string(timestamp());
		archive(timestamp_string);
        archive(sensor_online_);
	}

	template < class Archive >
	void load(Archive & archive) {
		std::string device;
		archive(device);
		sensor_reading_device_id(device);
		archive(temperature_);
		// timestamp
		std::string timestamp_string;
		archive(timestamp_string);
		timestamp(boost::posix_time::time_from_string(timestamp_string));
        archive(sensor_online_);
	}

};
#pragma db view object(temperature_reading)
    struct temperature_reading_count {
#pragma db column("COUNT(" + temperature_reading::temperature_reading_id_.sensor_reading_device_id_ + ")")
        std::size_t value;
        
    };

} // ns sensors
