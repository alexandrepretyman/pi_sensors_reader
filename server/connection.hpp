#pragma once
//
// connection.hpp
// ~~~~~~~~~~~~~~
//



#include <array>
#include <memory>
#include <boost/asio.hpp>
#include <cinttypes>
#include <odb/database.hxx>
#include <odb/transaction.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/mysql/database.hxx>
#include <memory>
#include "sensor_reading_device.hpp"
#include "generated-odb/sensor_reading_device-odb.hxx"
#include "generated-odb/temperature_reading-odb.hxx"
namespace server {

class connection_manager;
    
    enum transmission_state {
        WAITING_INITIAL_SIZE,
        WAITING_DEVICE_AND_READINGS,
        WAITING_SIZE,
        WAITING_READINGS,
    };

/// Represents a single connection from a client.
class connection
  : public std::enable_shared_from_this<connection>
{
public:
  connection(const connection&) = delete;
  connection& operator=(const connection&) = delete;

  /// Construct a connection with the given socket.
  explicit connection(boost::asio::ip::tcp::socket socket,
                      connection_manager& manager, std::shared_ptr<odb::mysql::database> db);

  /// Start the first asynchronous operation for the connection.
  void start();

  /// Stop all asynchronous operations associated with the connection.
  void stop();

private:
  /// Perform an asynchronous read operation.
  void do_read();

  /// Perform an asynchronous write operation.
  void disconnect();
    
    void read_device_and_readings();

    void create_if_doesnt_exist(sensors::sensor_reading_device & device);
    
    void persist(std::vector<sensors::temperature_reading> const & tr);
    
    void treat_incoming_streambuf();
    
    void read_readings();
    
    void send_done();

  /// Socket for the connection.
  boost::asio::ip::tcp::socket socket_;

  /// The manager for this connection.
  connection_manager& connection_manager_;

  /// The handler used to process the incoming request.
  std::shared_ptr<odb::mysql::database> db_;

  /// StreamBuffer for incoming data.
  boost::asio::streambuf socket_streambuf_;
    
    boost::asio::streambuf streambuf_;

    transmission_state state_;
    
    uint32_t data_to_receive_;
    uint32_t bytes_to_wait_on_socket_;
};

typedef std::shared_ptr<connection> connection_ptr;

} // namespace server
