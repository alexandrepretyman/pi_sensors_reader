//
// main.cpp
// ~~~~~~~~
//

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include "server.hpp"
#include <memory>
#include <odb/mysql/database.hxx>

#include <odb/database.hxx>
#include <odb/transaction.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/mysql/database.hxx>
#include <memory>
#include "sensor_reading_device.hpp"
#include "generated-odb/sensor_reading_device-odb.hxx"
#include "generated-odb/temperature_reading-odb.hxx"

int main(int argc, char* argv[])
{
  try
  {
    // Check command line arguments.
 //   if (argc != 4)
 //   {
 //     std::cerr << "Usage: http_server <address> <port> <doc_root>\n";
 //     std::cerr << "  For IPv4, try:\n";
 //     std::cerr << "    receiver 0.0.0.0 80 .\n";
 //     std::cerr << "  For IPv6, try:\n";
 //     std::cerr << "    receiver 0::0 80 .\n";
 //     return 1;
 //   }

     // Initialization of database: http://www.codesynthesis.com/products/odb/doc/manual.xhtml#17.2
      std::shared_ptr<odb::mysql::database> db = std::make_shared<odb::mysql::database>("dbusername", "dbpassword", "sensors", "192.168.109.131", 3306);
      
      
      odb::transaction t(db->begin());
      sensors::sensor_reading_device device;
      device.textual_id_ = "Device textual id";
      if (!db->find<sensors::sensor_reading_device>(device.textual_id_, device)) {
          db->persist(device);
      }
      t.commit();
      
    // Initialise the server.
//    server::server s(argv[1], argv[2], db);
    server::server s("0.0.0.0", "7001", db);

    // Run the server until stopped.
    s.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "exception: " << e.what() << "\n";
  }

  return 0;
}
