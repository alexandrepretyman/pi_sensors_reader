//
// connection.cpp
// ~~~~~~~~~~~~~~
//

#include "connection.hpp"
#include <utility>
#include <vector>
#include "connection_manager.hpp"
#include <cereal/archives/xml.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

namespace server {

connection::connection(boost::asio::ip::tcp::socket socket,
    connection_manager& manager, std::shared_ptr<odb::mysql::database> db)
  : socket_(std::move(socket)),
    connection_manager_(manager),
    db_(db),
    state_(transmission_state::WAITING_INITIAL_SIZE),
    data_to_receive_(0),
    bytes_to_wait_on_socket_(sizeof(uint32_t))
{
}

void connection::start()
{
  do_read();
}

void connection::stop()
{
  socket_.close();
}

void connection::do_read()
{
  auto self(shared_from_this());

    auto buffer = socket_streambuf_.prepare(bytes_to_wait_on_socket_);
  socket_.async_read_some(buffer,
      [this, self](boost::system::error_code ec, std::size_t bytes_transferred)
      {
          if (ec) {
              disconnect();
          }
          else {
              socket_streambuf_.commit(bytes_transferred);
              try {
                  treat_incoming_streambuf();
                  
              }
              catch (std::exception & ex) {
                  std::cout << "Exception: " << ex.what() << std::endl;
                  disconnect();
              }
          }
      });
}


void connection::treat_incoming_streambuf() {
              if (state_ == transmission_state::WAITING_INITIAL_SIZE) {
                  if (socket_streambuf_.size() < sizeof(uint32_t)) {
                      do_read();
                  }
                  else {
                      std::istream istream(&socket_streambuf_);
                      istream.read((char *) &data_to_receive_, sizeof (uint32_t));
                      bytes_to_wait_on_socket_ = data_to_receive_;
                      state_ = transmission_state::WAITING_DEVICE_AND_READINGS;
                  }

              }
              if (state_ == transmission_state::WAITING_DEVICE_AND_READINGS) {
                  if (socket_streambuf_.size() < data_to_receive_) {
                      do_read();
                  }
                  else {
                      read_device_and_readings();
                      bytes_to_wait_on_socket_ = sizeof(uint32_t);
                      state_ = transmission_state::WAITING_SIZE;
                  }
             }
              if (state_ == transmission_state::WAITING_SIZE) {
                  if (socket_streambuf_.size() < sizeof(uint32_t)) {
                      do_read();
                  }
                  else {
                      std::istream istream(&socket_streambuf_);
                      istream.read((char *) &data_to_receive_, sizeof (uint32_t));
                      if (data_to_receive_ == 0) {
                          send_done();
                      }
                      else {
                          bytes_to_wait_on_socket_ = data_to_receive_;
                          state_ = transmission_state::WAITING_READINGS;
                      }
                  }
              }
              if (state_ == transmission_state::WAITING_READINGS) {
                  if (socket_streambuf_.size() < bytes_to_wait_on_socket_) {
                      do_read();
                  }
                  else {
                      read_readings();
                      bytes_to_wait_on_socket_ = sizeof (uint32_t);
                      state_ = transmission_state::WAITING_SIZE;
                      treat_incoming_streambuf();
                  }

              }
}
    
    void connection::send_done() {
        {
            std::ostream ostream(&socket_streambuf_);
            uint32_t done = 1;
            ostream.write((char *) &done, sizeof(uint32_t));
        }
        socket_.async_send(socket_streambuf_.data(), [=](boost::system::error_code ec, std::size_t) {
          boost::system::error_code ignored_ec;
          socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both,
            ignored_ec);
            disconnect();
        });
    }
void connection::read_readings() {
    std::istream istream(&socket_streambuf_);
    cereal::XMLInputArchive archive(istream);
    
    std::vector<sensors::temperature_reading> v;
    archive(v);

    persist(v);
    
}
void connection::disconnect()
{
    connection_manager_.stop(shared_from_this());
}
    
    void connection::read_device_and_readings() {
        std::istream istream(&socket_streambuf_);
        cereal::XMLInputArchive archive(istream);
        
        sensors::sensor_reading_device device;
        archive(device);
       
        create_if_doesnt_exist(device);
        std::vector<sensors::temperature_reading> v;
        archive(v);

        persist(v);
        
        
    }

    void connection::create_if_doesnt_exist(sensors::sensor_reading_device & device) {
        odb::transaction t(db_->begin());
        if (!db_->find<sensors::sensor_reading_device>(device.textual_id_, device)) {
            db_->persist(device);
        }
        t.commit();
    }
    void connection::persist(std::vector<sensors::temperature_reading> const & tr) {
        std::cout << "Persisted " << tr.size() << " readings" << std::endl;;
        odb::transaction t(db_->begin());
       
        for (auto & reading : tr) {
            db_->persist(reading);
        }
        t.commit();
    }
} // namespace server
