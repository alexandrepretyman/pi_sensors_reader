// This file was generated by ODB, object-relational mapping (ORM)
// compiler for C++.
//

namespace odb
{
  // temperature_reading
  //

  inline
  access::object_traits< ::sensors::temperature_reading >::id_type
  access::object_traits< ::sensors::temperature_reading >::
  id (const object_type& o)
  {
    return o.temperature_reading_id_;
  }

  inline
  void access::object_traits< ::sensors::temperature_reading >::
  callback (database& db, object_type& x, callback_event e)
  {
    ODB_POTENTIALLY_UNUSED (db);
    ODB_POTENTIALLY_UNUSED (x);
    ODB_POTENTIALLY_UNUSED (e);
  }

  inline
  void access::object_traits< ::sensors::temperature_reading >::
  callback (database& db, const object_type& x, callback_event e)
  {
    ODB_POTENTIALLY_UNUSED (db);
    ODB_POTENTIALLY_UNUSED (x);
    ODB_POTENTIALLY_UNUSED (e);
  }

  // temperature_reading_count
  //

  inline
  void access::view_traits< ::sensors::temperature_reading_count >::
  callback (database& db, view_type& x, callback_event e)
  {
    ODB_POTENTIALLY_UNUSED (db);
    ODB_POTENTIALLY_UNUSED (x);
    ODB_POTENTIALLY_UNUSED (e);
  }
}

namespace odb
{
  // temperature_reading_id
  //

  inline
  bool access::composite_value_traits< ::sensors::temperature_reading_id, id_sqlite >::
  get_null (const image_type& i)
  {
    bool r (true);
    r = r && i.sensor_reading_device_id_null;
    r = r && i.timestamp_null;
    return r;
  }

  inline
  void access::composite_value_traits< ::sensors::temperature_reading_id, id_sqlite >::
  set_null (image_type& i,
            sqlite::statement_kind sk)
  {
    ODB_POTENTIALLY_UNUSED (sk);

    using namespace sqlite;

    i.sensor_reading_device_id_null = true;
    i.timestamp_null = true;
  }

  // temperature_reading
  //

  inline
  void access::object_traits_impl< ::sensors::temperature_reading, id_sqlite >::
  erase (database& db, const object_type& obj)
  {
    callback (db, obj, callback_event::pre_erase);
    erase (db, id (obj));
    callback (db, obj, callback_event::post_erase);
  }

  inline
  void access::object_traits_impl< ::sensors::temperature_reading, id_sqlite >::
  load_ (statements_type& sts,
         object_type& obj,
         bool)
  {
    ODB_POTENTIALLY_UNUSED (sts);
    ODB_POTENTIALLY_UNUSED (obj);
  }

  // temperature_reading_count
  //
}

