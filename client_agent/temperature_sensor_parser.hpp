#pragma once

#include <string>
#include <fstream>
#include <limits>
#include <cstdlib>

struct temperature_sensor_parser {

    temperature_sensor_parser(std::string const & filename) :  filename_(filename){

    }

    void read(bool & sensor_working, float & temperature) {
        sensor_working = false;
        temperature = std::numeric_limits<float>::min();

        std::ifstream ifstream(filename_);
        if (ifstream) {
            std::string first_line;
            std::getline(ifstream, first_line );
            if (has_working_sensor(first_line) && ifstream) {
                std::string second_line;
                std::getline(ifstream, second_line);
                parse_temperature(second_line, temperature);
                sensor_working = true;
            }
        }


    }

private:
    bool has_working_sensor(std::string const & first_line) {
       return first_line.find_last_of("YES") != std::string::npos;
    }


    void parse_temperature(std::string const & second_line, float & temperature) {

        std::size_t found_position = second_line.find_last_of("t=");
        std::string temperature_as_string = second_line.substr(found_position + 1);
        temperature = std::atof(temperature_as_string.c_str()) / 1000;

    }
    std::string const & filename_;
};
