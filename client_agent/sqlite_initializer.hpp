#pragma once
#include <odb/database.hxx>
#include <odb/transaction.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/sqlite/database.hxx>

struct sqlite_initializer {
    sqlite_initializer(std::shared_ptr<odb::sqlite::database> db) {
        odb::sqlite::connection_ptr c (db->connection ());
        c->execute ("PRAGMA foreign_keys=OFF");

        int rowCount = c->execute("SELECT * FROM main.sqlite_master WHERE name LIKE '%sensor%' AND type='table';");
        if( rowCount < 1 ){
            odb::transaction t (db->begin());
            //odb::schema_catalog::create_schema(*db, "v1");
            odb::schema_catalog::create_schema(*db);
            t.commit();
        }
        c->execute ("PRAGMA foreign_keys=ON");
    }
};
