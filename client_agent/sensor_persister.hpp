#pragma once
#include <string>
#include <boost/date_time/posix_time/ptime.hpp>
#include "sensors/sensor_reading_device.hpp"
#include "sensors/temperature_reading.hpp"
#include <odb/database.hxx>
#include <odb/transaction.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/sqlite/database.hxx>
#include "generated-odb/sensor_reading_device-odb.hxx"
#include "generated-odb/temperature_reading-odb.hxx"
#include <memory>
struct sensor_persister {
    sensor_persister(std::shared_ptr<odb::sqlite::database> db, std::string const & textual_id) : db_(db), textual_id_(textual_id) {}


    void init() {
        odb::transaction t(db_->begin());
        using query = odb::query<sensors::sensor_reading_device>;
        using result = odb::result<sensors::sensor_reading_device>;
        query q(query::textual_id == textual_id_);

        result r(db_->query<sensors::sensor_reading_device>(q));
        if (r.empty()) {
            sensor_reading_device_.textual_id_ = textual_id_;
            db_->persist(sensor_reading_device_);
        }
        else {
            db_->load<sensors::sensor_reading_device>(textual_id_, sensor_reading_device_);
        }
        t.commit();
    }

    void persist(boost::posix_time::ptime const & time, bool const sensor_ok, float const temperature) {

        odb::transaction t(db_->begin());
        sensors::temperature_reading reading;
        reading.sensor_reading_device_id(sensor_reading_device_.textual_id_);
        reading.temperature(temperature);
        reading.sensor_online(sensor_ok);
        reading.timestamp(time);

        db_->persist(reading);
        t.commit();
    }

    std::shared_ptr<odb::sqlite::database> db_;
    std::string textual_id_;

    sensors::sensor_reading_device sensor_reading_device_;
};
