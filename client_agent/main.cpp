#include <iostream>
#include <boost/asio.hpp>
#include <memory>
#include "periodic_sensor_persister.hpp"
#include "periodic_sensor_sender.hpp"
#include "sqlite_initializer.hpp"

namespace asio = boost::asio;

int main(int argc, char *argv[]) {
    try {
        asio::io_service io_service;
//        std::string hostname = "192.168.109.131";
        std::string hostname = "127.0.0.1";
        std::string port = "7001";
        unsigned int interval = 2;
        std::string filename = "temperature_device";
        std::string device_id = "Device textual id";

        /*  udp::socket s(io_service, udp::endpoint(udp::v4(), 0));*/

//        udp::resolver resolver(io_service);
//        udp::endpoint endpoint = *resolver.resolve(
//                {udp::v4(), hostname, port, asio::ip::udp::resolver::query::canonical_name});
//
//        //ftp::client::client client(io_service, endpoint);
//
//        ftp::client::connection con(io_service, endpoint);

//        con.start_async();

        std::shared_ptr<odb::sqlite::database> db = std::make_shared<odb::sqlite::database>("test.db", SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE);
        sqlite_initializer init(db);
        periodic_sensor_persister sensor_persister(db, io_service, interval, filename, device_id);
        sensor_persister.init_async();
        periodic_sensor_sender sensor_sender(db, io_service, hostname, port, device_id);
        sensor_sender.init_async();

        io_service.run();

    }
    catch (std::exception &e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}