#pragma once

#include <iosfwd>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>
#include <odb/database.hxx>
#include <odb/transaction.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/sqlite/database.hxx>
#include "generated-odb/sensor_reading_device-odb.hxx"
#include "generated-odb/temperature_reading-odb.hxx"
#include <iostream>
#include <cinttypes>
#include <chrono>

#include <cereal/archives/xml.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

using boost::asio::ip::tcp;
namespace asio = boost::asio;

enum class periodic_sensor_sender_state : unsigned char {
    INITIALIZING,
    OFFLINE,
    CONNECTING,
    CONNECTED,
    SENDING_DATA,
    WAITING_SHUTDOWN_ACK,
};

using cereal_serializer = cereal::XMLOutputArchive;

struct periodic_sensor_sender {
    periodic_sensor_sender(std::shared_ptr<odb::sqlite::database> db, boost::asio::io_service & io_service, std::string const & hostname, std::string const & port,
    std::string const & device_id)
        :  db_(db),
            io_service_(io_service),
        hostname_(hostname), port_(port),
           device_id_(device_id),
            state_(periodic_sensor_sender_state::INITIALIZING),
            timer_(io_service),
            socket_(io_service),
            ostream_(&streambuf_),
            istream_(&streambuf_)
        {

    }


    void init_async() {
        io_service_.post([=](){
            if (state_ == periodic_sensor_sender_state::INITIALIZING) {
                load_or_create_device();
                state_ = periodic_sensor_sender_state::OFFLINE;
            }
            check_and_send_data();
        });
    }
    
    void check_and_send_data() {
        if (state_ == periodic_sensor_sender_state::OFFLINE) {
            if (device_has_temperature_readings()) {
                state_ = periodic_sensor_sender_state::CONNECTING;
                try_to_connect();
            }
            else {
                reschedule_connect();
            }
        }
    }

    void try_to_connect() {
        tcp::resolver resolver(io_service_);
        tcp::resolver::iterator not_found;
        auto resolve_result = resolver.resolve({ hostname_, port_, tcp::resolver::query::canonical_name });
        
        if (resolve_result == not_found) {
            std::cout << "Could not resolve " << hostname_ << " trying in " << "30" << " seconds" << std::endl;
            state_ = periodic_sensor_sender_state::OFFLINE;
            reschedule_connect();
        }
        else {
            socket_.async_connect(*resolve_result, [=](boost::system::error_code const & error_code) {
                if (error_code) {
                    state_ = periodic_sensor_sender_state::OFFLINE;
                    std::cerr << error_code.message() << std::endl;
                    reschedule_connect();
                    socket_.close();
                }
                else {
                    reset_streambuf();
                    state_ = periodic_sensor_sender_state::CONNECTED;
                    {
                        cereal_serializer archive(ostream_);
                        read_device_into_streambuf(archive);
                        read_sensor_readings_into_streambuf(archive);
                     }
                     move_data_to_socket_stream_buf();
                     send_data();
                }
            });
        }
        
    }
    
    void move_data_to_socket_stream_buf() {
        std::cout << "Start" << std::endl;
        std::cout << socket_streambuf_.size() << " socket " << std::endl;
        std::cout << streambuf_.size() << " size after " << std::endl;
        std::ostream sostream(&socket_streambuf_);
        uint32_t size = streambuf_.size();
        sostream.write((char *) &size, sizeof(uint32_t));
        sostream << &streambuf_;
        
        std::cout << socket_streambuf_.size() << " socket " << std::endl;
        std::cout << streambuf_.size() << " size after " << std::endl;
        std::cout << "End" << std::endl;
    }
    
    void send_data() {
        state_ = periodic_sensor_sender_state::SENDING_DATA;
        socket_.async_send(socket_streambuf_.data(), [=](boost::system::error_code const & error, // Result of operation.
                                                  std::size_t bytes_transferred ) {
            if (error) {
                socket_.close();
                state_ = periodic_sensor_sender_state::OFFLINE;
                reschedule_connect();
            }
            else {
                socket_streambuf_.consume(bytes_transferred);
                if (socket_streambuf_.size() > 0) {
                    send_data();
                }
                else {
                    state_ = periodic_sensor_sender_state::CONNECTED;
                    if (count_temperature_readings() == 0) {
                        send_done();
                    }
                    else {
                        send_readings();
                    }
                }
            }
       });
    }
    
    void send_readings() {
        {
            cereal_serializer archive(ostream_);
            read_sensor_readings_into_streambuf(archive);
        }
        move_data_to_socket_stream_buf();
        send_data();
    }
    
    void send_done() {
        std::ostream sostream(&socket_streambuf_);
        uint32_t size = 0;
        sostream.write((char *) &size, sizeof(uint32_t));
        
        socket_.async_send(socket_streambuf_.data(), [=](boost::system::error_code const & error, // Result of operation.
                                                  std::size_t bytes_transferred ) {
            state_ = periodic_sensor_sender_state::WAITING_SHUTDOWN_ACK;
            auto bufs = socket_streambuf_.prepare(sizeof(uint32_t));
            socket_.async_read_some(bufs, [=](boost::system::error_code ec, std::size_t bytes_transferred)
                                    {
                                        state_ = periodic_sensor_sender_state::OFFLINE;
                                        
                                                  boost::system::error_code ignored_ec;
          socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both,
            ignored_ec);
                                        reschedule_connect();
                                        
                                    }
                                    );
       });
    }
    
    template < typename Archive >
    void read_device_into_streambuf(Archive & archive) {
        archive(sensor_reading_device_);
    }

    template < typename Archive >
    void read_sensor_readings_into_streambuf(Archive & archive) {
        odb::transaction t(db_->begin());
        using query = odb::query<sensors::temperature_reading>;
        using result = odb::result<sensors::temperature_reading>;
        
        query q("ORDER BY" + query::temperature_reading_id.timestamp + "ASC LIMIT " + std::to_string(reading_buffer_num));
        result r(db_->query<sensors::temperature_reading>(q));
        std::vector<sensors::temperature_reading> v;
        for (auto & tr : r){
            v.push_back(tr);
            db_->erase<sensors::temperature_reading>(tr);
        }
        archive(v);

        t.commit();
    }
    
    

    void load_or_create_device() {
        odb::transaction t(db_->begin());
        using query = odb::query<sensors::sensor_reading_device>;
        using result = odb::result<sensors::sensor_reading_device>;
        query q(query::textual_id == device_id_);

        result r(db_->query<sensors::sensor_reading_device>(q));
        if (r.empty()) {
            sensor_reading_device_.textual_id_ = device_id_;
            db_->persist(sensor_reading_device_);
        }
        else {
            db_->load<sensors::sensor_reading_device>(device_id_, sensor_reading_device_);
        }
        t.commit();
    }
    
    std::size_t count_temperature_readings() {
        odb::transaction t(db_->begin());
        using query = odb::query<sensors::temperature_reading_count>;
        using result = odb::result<sensors::temperature_reading_count>;
        query q(query::temperature_reading_id.sensor_reading_device_id == device_id_);

        result r(db_->query<sensors::temperature_reading_count>(q));
        auto const count = (*r.begin()).value;
        t.commit();
        return count;
    }
    
    bool device_has_temperature_readings() {
       return count_temperature_readings() > 0;
    }
    
    void reset_streambuf() {
//        streambuf_.consume(streambuf_.size());
        socket_streambuf_.consume(socket_streambuf_.size());
    }
    
    void reschedule_connect() {
        timer_.expires_from_now(std::chrono::seconds(3));
        timer_.async_wait([=](const boost::system::error_code& ec){
            if (!ec) {
                check_and_send_data();
            }
        });    
    }
    
    std::shared_ptr<odb::sqlite::database> db_;
    boost::asio::io_service & io_service_;
    std::string const & hostname_;
    std::string const & port_;

    std::string const & device_id_;
    sensors::sensor_reading_device sensor_reading_device_;
    periodic_sensor_sender_state state_;
    asio::steady_timer timer_;
    tcp::socket socket_;
    boost::asio::streambuf streambuf_;
    boost::asio::streambuf socket_streambuf_;
    std::ostream ostream_;
    std::istream istream_;
    std::size_t reading_buffer_num = {2};
};

