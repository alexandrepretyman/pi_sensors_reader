#pragma once
#include <string>
#include "client_agent/temperature_sensor_parser.hpp"
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <chrono>
#include "sensor_persister.hpp"
#include <iostream>
namespace asio = boost::asio;
struct periodic_sensor_persister {
    periodic_sensor_persister(std::shared_ptr<odb::sqlite::database> db, asio::io_service & io_service, unsigned int seconds_interval, std::string const & sensor_filename,
    std::string const & device_name)
    : io_service_(io_service),
      seconds_interval_(seconds_interval),
      temperature_sensor_parser_(sensor_filename),
      sensor_persister_(db, device_name),
      timer_(io_service)
        {}


    void run_async() {
        io_service_.post([=](){
            read_and_persist_temperature();
            reschedule();
        });
    }

    void init_async() {
         io_service_.post([=](){
            sensor_persister_.init();
            run_async();
        });
    }

    void reschedule() {
        timer_.expires_from_now(std::chrono::seconds(seconds_interval_));
        timer_.async_wait([=](const boost::system::error_code& ec){
            if (!ec) {
                run_async();
            }
        });
    }

    void read_and_persist_temperature() {
        boost::posix_time::ptime timestamp = boost::posix_time::second_clock::universal_time();
        float temperature = 2.0;
        bool working = true;
        temperature_sensor_parser_.read(working, temperature);
        sensor_persister_.persist(timestamp, working, temperature);
    }

    asio::io_service & io_service_;
    unsigned int seconds_interval_;
    temperature_sensor_parser temperature_sensor_parser_;
    sensor_persister sensor_persister_;

    asio::steady_timer timer_;
};
