#include <cereal/archives/xml.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include "sensors/sensor_reading_device.hpp"
#include "sensors/temperature_reading.hpp"
#include <sstream>
int main()
{
    std::stringstream ss; // any stream can be used

    {
        cereal::XMLOutputArchive oarchive(ss); // Create an output archive

        sensors::sensor_reading_device_ptr m1 = std::make_shared<sensors::sensor_reading_device>();
        m1->textual_id_ = "Hello world";
        oarchive(m1); // Write the data to the archive

        sensors::temperature_reading r;
        std::vector<sensors::temperature_reading> v;
        r.sensor_reading_device_id(m1->textual_id_);
        r.timestamp(boost::posix_time::ptime(boost::posix_time::second_clock::universal_time()));
        r.temperature(27.3);
        v.push_back(r);
        oarchive(v);


    }

    std::cout << ss.str() << std::endl;
    std::stringstream ss2; // any stream can be used
    {
        cereal::XMLInputArchive iarchive(ss); // Create an input archive

        sensors::sensor_reading_device_ptr m1;
        iarchive(m1); // Read the data from the archive

        sensors::temperature_reading r;
        std::vector<sensors::temperature_reading> v;
        iarchive(v);


        cereal::XMLOutputArchive oarchive(ss2); // Create an output archive

        oarchive(m1);
        oarchive(v);

    }
    std::cout << ss2.str() << std::endl;
}